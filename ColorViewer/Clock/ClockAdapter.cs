﻿using Java.Lang;
using Fragment = Android.Support.V4.App.Fragment;
using FragmentManager = Android.Support.V4.App.FragmentManager;

namespace Clock
{
    public class ClockAdapter : Android.Support.V4.App.FragmentPagerAdapter
    {
        private Fragment[] fragments;
        private ICharSequence[] titles;

        public ClockAdapter(FragmentManager fm, Fragment[] fragments, ICharSequence[] titles) : base(fm)
        {
            this.fragments = fragments;
            this.titles = titles;
        }

        public override int Count => fragments.Length;
        public override Fragment GetItem(int position) => fragments[position];

        public override ICharSequence GetPageTitleFormatted(int position) => titles[position];

    }
}