﻿using System;
using System.Timers;
using Android.OS;
using Android.Views;
using Android.Widget;

namespace Clock
{
    public class StopWatchFragment : Android.Support.V4.App.Fragment
    {
        private TextView timeTextView;
        private Button startStopButton;
        private Button resetButton;
        private Timer timer;
        private TimeSpan ticks;

        public StopWatchFragment()
        {
            timer = new Timer(1000);
            timer.Elapsed += OnElapsed;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = inflater.Inflate(Resource.Layout.fragment_stopwatch, container, false);

            timeTextView = view.FindViewById<TextView>(Resource.Id.timeTextView);
            startStopButton = view.FindViewById<Button>(Resource.Id.startStopButton);
            resetButton = view.FindViewById<Button>(Resource.Id.resetButton);

            startStopButton.Click += OnStartStop;
            resetButton.Click += OnReset;

            return view;
        }

        private void OnStartStop(object sender, EventArgs e)
        {
            if (startStopButton.Text == "Start")
            {
                startStopButton.Text = "Stop";
                timer.Start();
            }
            else
            {
                startStopButton.Text = "Start";
                timer.Stop();
            }
        }

        private void OnReset(object sender, EventArgs e)
        {
            timer.Stop();
            timeTextView.Text = "0:00:00";
            startStopButton.Text = "Start";
            ticks = TimeSpan.Zero;
        }

        private void OnElapsed(object sender, ElapsedEventArgs e)
        {
            ticks = ticks.Add(TimeSpan.FromSeconds(1));
            base.Activity.RunOnUiThread(() => timeTextView.Text = ticks.ToString("g"));
        }
    }
}