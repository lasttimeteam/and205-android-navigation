﻿using System;
using System.Timers;
using Android.OS;
using Android.Views;
using Android.Widget;

namespace Clock
{
    public class TimeFragment : Android.Support.V4.App.Fragment
    {
        private TextView timeTextView;
        private Timer timer;

        public TimeFragment()
        {
            timer = new Timer(1000);
            timer.Elapsed += OnElapsed;
            timer.Start();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            var view = inflater.Inflate(Resource.Layout.fragment_time, container, false);
            timeTextView = view.FindViewById<TextView>(Resource.Id.timeTextView);
            timeTextView.Text = DateTime.Now.ToString("T");

            return view;
        }

        private void OnElapsed(object sender, ElapsedEventArgs e)
        {
            base.Activity.RunOnUiThread(() => timeTextView.Text = DateTime.Now.ToString("T"));
        }
    }
}