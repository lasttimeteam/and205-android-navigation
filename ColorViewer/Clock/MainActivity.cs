﻿using Android.App;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Views;

namespace Clock
{
    [Activity(Label = "Clock", MainLauncher = true)]
    public class MainActivity : Android.Support.V7.App.AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_menu_white_24dp);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);

            var menu = FindViewById<NavigationView>(Resource.Id.navigationView);
            menu.NavigationItemSelected += OnMenuItemSelected;

            Navigate(new TimeFragment());
        }

        private void OnMenuItemSelected(object sender, NavigationView.NavigationItemSelectedEventArgs e)
        {
            switch (e.MenuItem.ItemId)
            {
                case Resource.Id.timeMenuItem: Navigate(new TimeFragment()); break;
                case Resource.Id.stopwatchMenuItem: Navigate(new StopWatchFragment()); break;
                case Resource.Id.aboutMenuItem: Navigate(new AboutFragment()); break;
            }

            e.MenuItem.SetChecked(true);

            var drawerLayout = FindViewById<Android.Support.V4.Widget.DrawerLayout>(Resource.Id.drawerLayout);
            drawerLayout.CloseDrawer(Android.Support.V4.View.GravityCompat.Start);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    var drawerLayout = FindViewById<Android.Support.V4.Widget.DrawerLayout>(Resource.Id.drawerLayout);
                    drawerLayout.OpenDrawer(Android.Support.V4.View.GravityCompat.Start);
                    break;
            }

            return true;
        }

        void Navigate(Android.Support.V4.App.Fragment fragment) => base.SupportFragmentManager.BeginTransaction()
                .Replace(Resource.Id.contentFrame, fragment)
                .Commit();
    }
}

