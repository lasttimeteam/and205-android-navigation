﻿using Android.OS;
using Android.Views;

namespace Clock
{
    public class AboutFragment : Android.Support.V4.App.Fragment
    {
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) 
            => inflater.Inflate(Resource.Layout.fragment_about, container, false);
    }
}