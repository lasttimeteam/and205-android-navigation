﻿using Android.App;
using Android.OS;
using Android.Widget;
using System;

namespace ColorViewer
{
    [Activity(Label = "@string/app_name", MainLauncher = true)]
    public class MainActivity : Android.Support.V4.App.FragmentActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            FindViewById<Button>(Resource.Id.buttonSliders).Click += ButtonSlidersClick;
            FindViewById<Button>(Resource.Id.buttonEditText).Click += ButtonEditTextClick;
        }

        private void ButtonSlidersClick(object sender, EventArgs e)
        {
            var fragment = new ColorsSliderFragment();

            SupportFragmentManager.BeginTransaction()
                .Replace(Resource.Id.frameLayout, fragment)
                .Commit();
        }

        private void ButtonEditTextClick(object sender, EventArgs e)
        {
            var fragment = new ColorsEditTextFragment();

            SupportFragmentManager.BeginTransaction()
                .Replace(Resource.Id.frameLayout, fragment)
                .Commit();
        }        
    }
}